build:
			docker build -t savnn/jdk11-awscli-alpine:latest .

release: build
			docker image push savnn/jdk11-awscli-alpine:latest
