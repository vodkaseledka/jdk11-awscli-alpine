FROM amazoncorretto:11-alpine

LABEL maintainer="alexander.smirnoff@gmail.com"

RUN set -eux; \
        apk --no-cache add python3 py3-pip make; \
        pip3 --no-cache-dir install -U awscli;
